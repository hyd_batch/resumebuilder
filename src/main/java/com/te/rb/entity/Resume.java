package com.te.rb.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class Resume {
	@Id
	private String resumeId;
	private String firstName;
	private String lastName;
	@OneToMany(mappedBy = "resume", cascade = CascadeType.ALL)
	private List<Technologies> technologies = Lists.newArrayList();
	private Double totalExperience;
	private Double relevantExperience;

	@OneToMany(mappedBy = "resume", cascade = CascadeType.ALL)
	private List<Summary> summary = Lists.newArrayList();

	@OneToOne(mappedBy = "resume", cascade = CascadeType.ALL)
	private Skills skills;

	@OneToOne(mappedBy = "resume", cascade = CascadeType.ALL)
	private Education education;

	@OneToMany(mappedBy = "resume", cascade = CascadeType.ALL)
	private List<ProjectDetails> projectDetails = Lists.newArrayList();
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Employee employee;

	@Lob
	private byte[] photo;

	@Lob
	private byte[] logo;
}

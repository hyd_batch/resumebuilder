package com.te.rb.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class Skills {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer sNo;
	
	@OneToOne(cascade = CascadeType.ALL)
	private Resume resume;
	
	@OneToMany(mappedBy = "skills",cascade = CascadeType.ALL)
	private List<FrontendTechnologies> frontendTechnologies = Lists.newArrayList();
	
	@OneToMany(mappedBy = "skills",cascade = CascadeType.ALL)
	private List<BackendTechnologies> backendTechnologies = Lists.newArrayList();
	
	@OneToMany(mappedBy = "skills",cascade = CascadeType.ALL)
	private List<MiddlewareTechnologies> middlewareTechnologies = Lists.newArrayList();
	
	@OneToMany(mappedBy = "skills",cascade = CascadeType.ALL)
	private List<DesignPatterns> designPatterns = Lists.newArrayList();
	
	@OneToMany(mappedBy = "skills",cascade = CascadeType.ALL)
	private List<DatabaseUsed> database = Lists.newArrayList();
	
	@OneToMany(mappedBy = "skills",cascade = CascadeType.ALL)
	private List<VersionControlSystem> versionControlSystems = Lists.newArrayList();
	
	@OneToMany(mappedBy = "skills",cascade = CascadeType.ALL)
	private List<AWS> aws = Lists.newArrayList();
	
	@OneToMany(mappedBy = "skills",cascade = CascadeType.ALL)
	private List<SDLC> sdlcs = Lists.newArrayList();
	
	@OneToMany(mappedBy = "skills",cascade = CascadeType.ALL)
	private List<DevelopmentTools> developmentTools = Lists.newArrayList();
}

package com.te.rb.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class ProjectDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer projectId;
	
	@OneToMany(mappedBy = "projectDetails",cascade = CascadeType.ALL)
	private List<FrontendTechnologies> frontendTechnologies = Lists.newArrayList();
	
	@OneToMany(mappedBy = "projectDetails",cascade = CascadeType.ALL)
	private List<BackendTechnologies> backendTechnologies = Lists.newArrayList();
	
	@OneToMany(mappedBy = "projectDetails",cascade = CascadeType.ALL)
	private List<DatabaseUsed> databases = Lists.newArrayList();
	
	@OneToMany(mappedBy = "projectDetails",cascade = CascadeType.ALL)
	private List<DesignPatterns> designPatterns = Lists.newArrayList();
	
	@OneToMany(mappedBy = "projectDetails",cascade = CascadeType.ALL)
	private List<DevelopmentTools> developmentTools = Lists.newArrayList();
	
	private String duration;
	
	private Integer teamSize;
	
	private String projectDescription;
	
	private String rolesAndResponsibilities;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private Resume resume;
}

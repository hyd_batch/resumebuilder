package com.te.rb.employee.service;

import java.util.Optional;

import com.te.rb.dto.RegistrationDto;

public interface EmployeeService {

	Optional<String> register(RegistrationDto registrationDto);

}

package com.te.rb.employee.service.implementation;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.te.rb.dto.RegistrationDto;
import com.te.rb.employee.service.EmployeeService;
import com.te.rb.entity.AppUser;
import com.te.rb.repository.AppUserRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService{

	private final AppUserRepository appUserRepository;
	
	@Override
	public Optional<String> register(RegistrationDto registrationDto) {
		AppUser appUser = new AppUser();
		appUser.setUsername(registrationDto.getEmployeeId());
		appUser.setPassword(registrationDto.getPassword());
		appUserRepository.save(appUser);
		return Optional.ofNullable(appUser.getUsername());
	}

}

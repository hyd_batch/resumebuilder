package com.te.rb.controller;

import java.util.Optional;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.te.rb.dto.RegistrationDto;
import com.te.rb.employee.service.EmployeeService;
import com.te.rb.exception.RegistrationFailedException;
import com.te.rb.response.GeneralResponse;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/employee")
public class EmployeeController {
	
	private final EmployeeService employeeService;
	
	@PostMapping(path = "/register")
	public GeneralResponse<String> employeeRegister(@RequestBody RegistrationDto registrationDto){
		Optional<String> employee = employeeService.register(registrationDto);
		if(employee.isPresent()) {
			return new GeneralResponse<String>("Employee Registration Successfull", employee.get());
		}
		throw new RegistrationFailedException("Employee Registration Unsuccessfull");
	}
	
	
	
}

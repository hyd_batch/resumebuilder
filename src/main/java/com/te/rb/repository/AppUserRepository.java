package com.te.rb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.te.rb.entity.AppUser;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, String>{
	
}

package com.te.rb.response;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GeneralResponse<T> {
	private String message;
	private T data;
	private LocalDateTime timeStamp = LocalDateTime.now();
	
	public GeneralResponse(String message) {
		super();
		this.message = message;
	}

	public GeneralResponse(String message, T data) {
		super();
		this.message = message;
		this.data = data;
	}
}

package com.te.rb.util;

import java.util.Random;

import org.springframework.stereotype.Component;
@Component
public class PasswordGenerator {
	
	public String passwordGenerator() {
		
		 String numbers = "123456789";

		 String Combination = numbers;
		 int length = 3;
		 char[] password = new char[length];
		 Random random = new Random();
		 for(int i = 0;i < length;i++) {
			 password[i] = Combination.charAt(random.nextInt(Combination.length()));
		 }
		 return  ""+password;
	}
}

package com.te.rb.dto;

import java.util.List;

import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProjectDetailsDto {
	
private List<FrontendTechnologiesDto> frontendTechnologiesDto = Lists.newArrayList();
	
	private List<BackendTechnologiesDto> backendTechnologiesDto = Lists.newArrayList();
	
	private List<DatabaseUsedDto> databasesDto = Lists.newArrayList();
	
	private List<DesignPatternsDto> designPatternsDto = Lists.newArrayList();
	
	private List<DevelopmentToolsDto> developmentToolsDto = Lists.newArrayList();
	
	private String duration;
	
	private Integer teamSize;
	
	private String projectDescription;
	
	private String rolesAndResponsibilities;
}

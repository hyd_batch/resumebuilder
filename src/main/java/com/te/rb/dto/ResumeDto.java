package com.te.rb.dto;

import java.util.List;

import javax.persistence.Lob;

import com.google.common.collect.Lists;
import com.te.rb.entity.Education;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ResumeDto {
	
	private String resumeId;
	private String firstName;
	private String lastName;
	
	private List<TechnologiesDto> technologiesDto = Lists.newArrayList();
	
	private Double totalExperience;
	
	private Double relevantExperience;
	
	private List<SummaryDto> summaryDto = Lists.newArrayList();

	private SkillsDto skillsDto;

	private Education education;

	private List<ProjectDetailsDto> projectDetailsDto = Lists.newArrayList();
	
	private EmployeeDto employeeDto;

	@Lob
	private byte[] photo;

	@Lob
	private byte[] logo;
}

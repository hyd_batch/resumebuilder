package com.te.rb.dto;

import java.util.List;

import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class SkillsDto {
	
	private List<FrontendTechnologiesDto> frontendTechnologiesDto = Lists.newArrayList();
	
	private List<BackendTechnologiesDto> backendTechnologiesDto = Lists.newArrayList();
	
	private List<MiddlewareTechnologiesDto> middlewareTechnologiesDto = Lists.newArrayList();
	
	private List<DesignPatternsDto> designPatternsDto = Lists.newArrayList();
	
	private List<DatabaseUsedDto> databaseDto = Lists.newArrayList();
	
	private List<VersionControlSystemDto> versionControlSystemsDto = Lists.newArrayList();
	
	private List<AWSDto> awsDto = Lists.newArrayList();
	
	private List<SDLCDto> sdlcsDto = Lists.newArrayList();
	
	private List<DevelopmentToolsDto> developmentToolsDto = Lists.newArrayList();
}
